const yargs = require('yargs');

const notes = require('./notes.js');

const argv = yargs
    .command('add', 'Add a new note', {
        title: cmdOption('Title of note', 't', true),
        body: cmdOption('Body of note', 'b', true)
    })
    .command('list', 'List all notes')
    .command('read', 'Read a note', {
        title: cmdOption('Title of note', 't', true)
    })
    .command(['remove', 'rm'], 'Remove a note', {
        title: cmdOption('Title of note', 't', true)
    })
    .help()
    .alias('help', 'h')
    .argv;
var command = argv._[0];

if (command === 'add') {
    var note = notes.addNote(argv.title, argv.body);
    if (note) {
        console.log('Note created!');
        notes.logNote(note);
    } else {
        lError('No note created!');
    }
} else if (command === 'list') {
    var allNotes = notes.getAll();
    console.log(`Printing ${allNotes.length} note(s).`)
    allNotes.forEach(note => notes.logNote(note));
} else if (command === 'read') {
    var argumentTitle = argv.title;
    var note = notes.getNote(argumentTitle);
    if (note) {
        console.log(`Note found!`);
        notes.logNote(note);
    } else {
        lError('Note not found!');
    }
} else if (command === 'remove' || 'rm') {
    var noteRemoved = notes.removeNote(argv.title);
    var message = noteRemoved ? 'Note was removed!' : 'ERROR: Note not found';
    console.log(message);
} else {
    lError('Command not recognized!');
}

function cmdOption (description, alias, demand) {
    return opt = {
        describe: description,
        alias,
        demand
    };
};

function lError (errorMessage) {
    console.log(`ERROR: ${errorMessage}`);
};