// var obj = {
//     name: 'Kai'
// };

// var stringObj = JSON.stringify(obj);

// console.log(typeof stringObj);
// console.log(stringObj);

// var personString = '{"name": "Kai","age": 23}';
// var person = JSON.parse(personString);

// console.log(typeof person);
// console.log(person);

const fs = require('fs');

var originalNote = {
    title: 'Some title',
    body: 'Some body'
};

var originalNoteString = JSON.stringify(originalNote); // Converts a json object to an string
fs.writeFileSync('notes.json', originalNoteString); // Write to file

var noteString = fs.readFileSync('notes.json'); // Read from file
var note = JSON.parse(noteString); // Converts a string to an json object

console.log(typeof note);
console.log(note.title);